﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace solucionexamen2
{
    public partial class Form1 : Form
    {

        double variable1;
        int operacion = 1;
        double grados = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            variable1 = double.Parse(textBox1.Text);
            operacion = 1;
            textBox1.Text = "";
        }

        private void button10_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "0";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "9";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            int longitud_total = textBox1.Text.Length-1;
            string letras="";
            for(int i=0;i<longitud_total;i++){
                letras = letras + textBox1.Text[i] + "";
            }
            textBox1.Text = letras;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            variable1 = double.Parse(textBox1.Text);
            operacion = 2;
            textBox1.Text = "";
        }

        private void button15_Click(object sender, EventArgs e)
        {
            variable1 = double.Parse(textBox1.Text);
            operacion = 3;
            textBox1.Text = "";
        }

        private void button17_Click(object sender, EventArgs e)
        {
            double resultado;
            if(operacion==1){
                resultado = variable1 + double.Parse(textBox1.Text);
                textBox1.Text = resultado+"";
            }
            else if (operacion == 2)
            {
                resultado = variable1 - double.Parse(textBox1.Text);
                textBox1.Text = resultado + "";
            }else if(operacion==3){
                resultado = variable1 * double.Parse(textBox1.Text);
                textBox1.Text = resultado+"";
            }

        }

        private void button16_Click(object sender, EventArgs e)
        {
            double resultado;
            resultado = double.Parse(textBox1.Text);
            resultado = Math.Sqrt(resultado);
            textBox1.Text = resultado + "";

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


            
            if (comboBox1.SelectedIndex + "" == "0")
            {
                groupBox1.Visible = true;
                groupBox2.Visible = false;

            }

            if (comboBox1.SelectedIndex + "" == "1")
            {
                groupBox1.Visible = true;
                groupBox2.Visible = true;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            double resultado;
            
            grados = double.Parse(textBox1.Text);
           


            if(radioButton2.Checked){
                
                grados = Math.PI * grados / 180.0;
            }
            
           
            resultado = Math.Sin(grados);
            textBox1.Text = resultado + "";
        }

        private void button19_Click(object sender, EventArgs e)
        {
            double resultado;

            grados = double.Parse(textBox1.Text);



            if (radioButton2.Checked)
            {

                grados = Math.PI * grados / 180.0;
            }


            resultado = Math.Cos(grados);
            textBox1.Text = resultado + "";
        }

        private void button20_Click(object sender, EventArgs e)
        {
            double resultado;

            grados = double.Parse(textBox1.Text);



            if (radioButton2.Checked)
            {

                grados = Math.PI * grados / 180.0;
            }


            resultado = Math.Tan(grados);
            textBox1.Text = resultado + "";
        }
    }
}
