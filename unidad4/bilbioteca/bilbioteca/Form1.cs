﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
namespace bilbioteca
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            if (File.Exists("ArchivoBinario.bibi")){
                using (FileStream fs = new FileStream("ArchivoBinario.bibi", FileMode.Open)){
                    using (StreamReader sr = new StreamReader(fs)){
                        fs.Position = 0;
                        using (BinaryReader br = new BinaryReader(fs)){
                            int numerodefilas = br.ReadInt32();
                            for (int i = 0; i < numerodefilas; i++){
                                int id = br.ReadInt32();
                                string nombre = br.ReadString();
                                string prestado = br.ReadString();
                                dataGridView1.Rows.Insert(0, id, nombre, prestado);
                            }
                        }
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string path = @"datos.txt";
            // Create a file to write to.
            using (StreamWriter sw = File.CreateText(path))
            {
                sw.WriteLine("Los libros y estado son:");
                for (int i = 0; i < dataGridView1.Rows.Count-1; i++)
                {
                    string id = dataGridView1.Rows[i].Cells[0].Value + "";
                    string nombre = dataGridView1.Rows[i].Cells[1].Value + "";
                    string prestado = dataGridView1.Rows[i].Cells[2].Value + "";
                    
                    sw.WriteLine("id : "+ id);
                    sw.WriteLine("El nombre del libro: "+nombre);
                    sw.WriteLine("El libro fue prestado? "+prestado);
                }
                MessageBox.Show("los datos se guardaron con EXITO!!!");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (FileStream fs = new FileStream("ArchivoBinario.bibi", FileMode.Create))
            {
                // Escritura sobre el archivo binario `ArchivoBinario.bin` usando un 
                // objeto de la clase `BinaryWriter`:
                using (BinaryWriter bw = new BinaryWriter(fs))
                {  //EL PROBLEMA EXISTE POR GUARDAR LA ULTIMA FILA, LA CUAL POSEE VALORES NULOS Y VACIOS
                    //POR LO QUE HAY QUE GUARDAR TODOS LOS DATOS DE LAS FILAS MENOS LA ULTIMA (dataGridView1.Rows.Count-1)
                    bw.Write(dataGridView1.Rows.Count - 1);
                    for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
                    {
                        int id = Int32.Parse(dataGridView1.Rows[i].Cells[0].Value + "");
                        string nombre = dataGridView1.Rows[i].Cells[1].Value + "";
                        string prestado = dataGridView1.Rows[i].Cells[2].Value + "";

                        bw.Write(id);
                        bw.Write(nombre);
                        bw.Write(prestado);
                    }
                }
                    

                    MessageBox.Show("datos guardados con exito");   
                }
        }
    }
}
