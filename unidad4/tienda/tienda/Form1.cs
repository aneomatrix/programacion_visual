﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;

namespace tienda
{
    public partial class Form1 : Form
    {

        
        public Form1()
        {
            InitializeComponent();
            ArrayList productos = new ArrayList();
            ArrayList precios = new ArrayList();
            

            if (File.Exists("ArchivoBinario.bin"))
            {
                int letter = 0;
                // Apertura del archivo `ArchivoBinario.bin` en modo lectura:
                using (FileStream fs = new FileStream("ArchivoBinario.bin", FileMode.Open))
                {
                    // Muestra la información tal cual está escrita en el archivo binario:
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        // Lectura y conversión de los datos binarios en el tipo de 
                        // correspondiente:

                        // Posiciona el cursor desde se iniciara la lectura del 
                        // archivo `ArchivoBinario`:
                        fs.Position = 0;

                        using (BinaryReader br = new BinaryReader(fs))
                        {
                            
                            int c = 0;
                            //try
                            //{
                            int numerodefilas = br.ReadInt32();
                            for (int i = 0; i < numerodefilas; i++)
                            {
                                int identificador = br.ReadInt32();

                                string nombre = br.ReadString();

                                int precio = br.ReadInt32();
                                dataGridView1.Rows.Insert(0, identificador, nombre, precio);
                                productos.Add(nombre);
                                precios.Add(precio);
                                chart1.Series[0].Points.DataBindXY(productos, precios);
                            }
                            //}
                            //catch
                            //{

                            //}
                        }
                    }
                }
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ArrayList productos = new ArrayList();
            ArrayList precios = new ArrayList();

            using (FileStream fs = new FileStream("ArchivoBinario.bin", FileMode.Create))
            {
                // Escritura sobre el archivo binario `ArchivoBinario.bin` usando un 
                // objeto de la clase `BinaryWriter`:
                using (BinaryWriter bw = new BinaryWriter(fs))
                {  //EL PROBLEMA EXISTE POR GUARDAR LA ULTIMA FILA, LA CUAL POSEE VALORES NULOS Y VACIOS
                    //POR LO QUE HAY QUE GUARDAR TODOS LOS DATOS DE LAS FILAS MENOS LA ULTIMA (dataGridView1.Rows.Count-1)
                    bw.Write(dataGridView1.Rows.Count-1);
                    for (int i = 0; i < dataGridView1.Rows.Count-1; i++){
                        int identificador = Int32.Parse(dataGridView1.Rows[i].Cells[0].Value+"");
                        string nombre = dataGridView1.Rows[i].Cells[1].Value + "";
                        int precio = Int32.Parse(dataGridView1.Rows[i].Cells[2].Value+"");
                        
                        bw.Write(identificador);
                        bw.Write(nombre);
                        bw.Write(precio);

                        productos.Add(nombre);
                        precios.Add(precio);
                        
                    }
                    chart1.Series[0].Points.DataBindXY(productos, precios);

                    MessageBox.Show("datos guardados con exito");   
                }
            }
        }
    }
}
