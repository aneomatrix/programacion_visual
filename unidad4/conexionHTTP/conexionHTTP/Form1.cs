﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.IO;

namespace conexionHTTP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sURL;
            sURL = "https://programacion-visual.herokuapp.com/";
            
            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(sURL);
            
            Stream objStream;
            objStream = wrGETURL.GetResponse().GetResponseStream();
            
            StreamReader objReader = new StreamReader(objStream);
            string sLine = "";
            int i = 0;
            
            while (sLine!=null){
                i++;
                sLine = objReader.ReadLine();
                if (sLine != null){
                    label1.Text = sLine;
                }
                    
            }


        }

        private void button2_Click(object sender, EventArgs e){

            string mytexto = textBox1.Text;
            string mynumero = numericUpDown1.Text;

            var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://programacion-visual.herokuapp.com/recibejson");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"texto\": \""+mytexto+"\"," +
                              "\"temperatura\":"+mynumero+"}";

                //string json = "{\"texto\":\"test\"," +
                  //            "\"password\":\"bla\"}";

                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                label3.Text = streamReader.ReadToEnd();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string sURL;
            sURL = "https://programacion-visual.herokuapp.com/regresajson";

            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(sURL);

            Stream objStream;
            objStream = wrGETURL.GetResponse().GetResponseStream();

            StreamReader objReader = new StreamReader(objStream);
            string sLine = "";
            int i = 0;

            while (sLine != null)
            {
                i++;
                sLine = objReader.ReadLine();
                if (sLine != null)
                {
                    label2.Text = sLine;
                }

            }
        }
    }
}
