﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            double gradosc=0;
            double f;

            //if ("hola" == "hola") { Console.WriteLine("Son iguales"); }

            Console.WriteLine("Ingresa un valor en grados centrigrados:");
            gradosc = Double.Parse(Console.ReadLine());
            
            f = (gradosc * (1.8)) + 32.0;

            Console.WriteLine("El valor en grados fahrenheit es :");
            Console.WriteLine(f);

            Console.Read();
        }
    }
}
