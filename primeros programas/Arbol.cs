﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gato
{
    class Arbol
    {
        public double tamaño;
        public string color;
        public int edad;
        public string nombre;

        public Arbol(){
            Console.WriteLine("inserte el nombre del arbol");
            nombre = Console.ReadLine();
            Console.WriteLine("inserte el tamaño del arbol");
            tamaño = Double.Parse(Console.ReadLine());
            Console.WriteLine("inserte el color del arbol");
            color = Console.ReadLine();
            Console.WriteLine("inserte la edad del arbol");
            edad = Int32.Parse(Console.ReadLine());
        }
        public Arbol(double tamaño, string color, int edad, string n){
            this.tamaño = tamaño;
            this.color = color;
            this.edad = edad;
            nombre = n;
        }

        public void mostrardatos()
        {
            Console.WriteLine("el objeto tiene esta edad: " + edad + " el objeto tiene este nombre: " + nombre + " el objeto  tiene este tamaño: " + tamaño);
        }

        public void crecer(){
            Console.WriteLine("toy creciendo");
        }

        public void tirarhojas(){
            Console.WriteLine("voy a aventar hojas");
        }

    }
}
