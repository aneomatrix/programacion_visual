﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gato
{
    class Program
    {
        static void Main(string[] args)
        {
            Persona juanito = new Persona("juanito", 18, "una casa");
            Persona paquito = new Persona("paquito", 19, "una calle");
           
            paquito.mostrardatos();
            paquito.dormir();

            juanito.mostrardatos();
            juanito.dormir();
            juanito.comer();

            Console.Read();
        }

    }
}
