﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static string convertirabinario(int numero){
            String cadena ="";
            while (numero > 0) {
                    if (numero % 2 == 0){
                        cadena = "0" + cadena;
                    }else{
                        cadena = "1" + cadena;
                    }
                    numero = (int)(numero / 2);
                }
            return cadena;
        }
        static void Main(string[] args){
            for (int valor = 1; valor <= 255; valor++){
                int numero = valor;
               Console.WriteLine(convertirabinario(numero));
            }
            Console.Read();
        }
    }
}
