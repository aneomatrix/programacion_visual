﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gato
{
    class Gato{
        public string nombre;
        public int edad;
        public string color;
        public string raza;

        public Gato(string nombre, int e, string c, string r){
            this.nombre = nombre;
            this.edad = e;
            color = c;
            raza = r;
        }

        public Gato(){
            Console.WriteLine("escribe el nombre del gato");
            nombre = Console.ReadLine();
            Console.WriteLine("escribe la edad del gato");
            edad = Int32.Parse(Console.ReadLine());
            Console.WriteLine("escribe el color del gato");
            color = Console.ReadLine();
            Console.WriteLine("escribe la raza del gato");
            raza = Console.ReadLine();
        }


        public void maullar()
        {
            Console.WriteLine("MIAU MIAU MIAU MIAU");
        }

        public void dormir()
        {
            Console.WriteLine("toy durmiendo");
        }

        public void pelear()
        {
            Console.WriteLine("$%&(&/()ggf6gf");
        }
    }
}
