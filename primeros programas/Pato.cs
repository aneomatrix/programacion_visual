﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gato
{
    class Pato
    {
        public string color;
        public int edad;
        public double peso;

        public Pato(string color, int edad, double peso){
            this.color = color;
            this.edad = edad;
            this.peso = peso;
        }
        public Pato(){
            Console.WriteLine("introduzca el color del pato");
            color = Console.ReadLine();
            Console.WriteLine("introduzca la edad del pato");
            edad = Int32.Parse(Console.ReadLine());
            Console.WriteLine("introduzca el peso del pato");
            peso = Double.Parse(Console.ReadLine());
        }
        public void comer(){
            Console.WriteLine("cuac, cuac, estoy cominendo"); }
        public void mostardatos(){
            Console.WriteLine("el color de este pato es :" + color+ " el peso es: "+peso+" la edad es: "+ edad); }
        public void nadar(){
            Console.WriteLine("cuac, cuac, me estoy ahogando"); }
        public void graznar(){
            Console.WriteLine("cuac, cuac, estoy graznando"); }
    }
}
